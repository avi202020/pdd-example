# language: en

Feature:

	Scenario: compa is unreachable
		Given serverless is running
		When it cannot reach compa
		Then a 5XX HTTP error is returned
		And a log entry of SEV_HIGH is recorded providing the request ID

	Scenario: compa has crashed unrecoverably
		Given serverless is running
		When compa crashes and does not recover
		And serverless tries to reach compa
		Then a 5XX error is returned
		And a log entry of SEV_HIGH is recorded providing the request ID

	Scenario: compa recovers from a crash
		Given serverless is running
		When compa crashes
		And compa tries to reach compa
		Then compa will restart within one second
		And serverless will try to reach compa again after one second
		And a successful response will be received
		And a log entry of SEV_MED is recorded for the crash
		And a log entry of SEV_INFO is recorded for the restart
