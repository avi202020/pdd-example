# language: en

Feature: ID file merging

	@story2.1
	@story2.2
	Scenario: Merging works
		Given two files each containing one user ID per line
		When I parse the two files, an output file is produced
		And the output file contains the union of the IDs from each of the two input files
		And the order of the IDs in the first file is preserved in the output file
		And in the output file, no user ID appears more than once.

	@story2.1
	Scenario: Order of IDs from first file is preserved

	@story2.2
	Scenario: Duplicate user IDs are prevented

	Scenario: One file is empty

	Scenario: Both files are empty

	Scenario: The two files are identical

	Scenario: The IDs in the two files do not overlap at allowed

	Scenario: IDs in the second file are the same as the first file, but in reverse order

	Scenario: A file contains more than one error

	Scenario: Both files contain an error

	Scenario: A file cannot be found

	Scenario: A file read error occurs
	
